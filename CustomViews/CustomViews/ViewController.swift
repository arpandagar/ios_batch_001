//
//  ViewController.swift
//  CustomViews
//
//  Created by Cerebro on 15/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var threeView: CKThreeImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        threeView.firstImageView.image = UIImage(named: "lion.jpg");
        threeView.secondImageView.image = UIImage(named: "shark.jpeg");
        threeView.thirdImageView.image = UIImage(named: "lion.jpg");
        
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

