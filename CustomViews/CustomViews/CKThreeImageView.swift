//
//  UIThreeImageView.swift
//  CustomViews
//
//  Created by Cerebro on 15/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class CKThreeImageView: UIView {

    @IBOutlet weak var firstImageView: UIImageView!
    
    @IBOutlet weak var secondImageView: UIImageView!
    
    @IBOutlet weak var thirdImageView: UIImageView!
    
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        NSBundle.mainBundle().loadNibNamed("CKThreeImageView", owner: self, options: nil)
        self.addSubview(firstImageView)
        self.addSubview(secondImageView)
        self.addSubview(thirdImageView)
    }

}
