//
//  FirstScreenViewController.swift
//  Segue
//
//  Created by Cerebro on 30/05/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class FirstScreenViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        var number = cell!.textLabel!.text!.toInt()!
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        var vc = storyboard.instantiateViewControllerWithIdentifier("second_screen") as! PahadaViewController
        
        vc.number = number
        
        showViewController(vc, sender: self)
     }
    
    
}

