//
//  PahadaViewController.swift
//  Segue
//
//  Created by Cerebro on 30/05/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class PahadaViewController: UIViewController {
    
    var number:Int = 0

    @IBOutlet weak var testLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        testLabel.text = "number is \(number)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
