//
//  Contact.swift
//  ContactsApp
//
//  Created by Cerebro on 12/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import Foundation

class CKContact {
    var userId:Int
    var name:String
    var phoneNumber:String?
    var email:String?
    
    init(userId:Int, name:String, phoneNumber:String?, email:String?){
        self.userId = userId
        self.name = name
        self.phoneNumber = phoneNumber
        self.email = email
    }
}
