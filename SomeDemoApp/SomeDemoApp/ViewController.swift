//
//  ViewController.swift
//  SomeDemoApp
//
//  Created by Cerebro on 17/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var gestureOutputTextLabel: UILabel!
    
    @IBOutlet weak var gestureView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: "tapRecognized")
        
        gestureView.addGestureRecognizer(tapGesture)
        
        
        
        
        let pinchGesture = UIPinchGestureRecognizer()
       pinchGesture.addTarget(self, action: "pinchReceived:")
        gestureView.addGestureRecognizer(pinchGesture)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }
    
    func tapRecognized() {
        gestureOutputTextLabel.text = "Tap received";
    }
    
    
    @IBAction func gestureRecognizer(sender: AnyObject) {
        
        gestureOutputTextLabel.text = "Rotation gesture received";
    }
    
    @IBAction func swipeReceived(sender: UISwipeGestureRecognizer) {
    }
    @IBAction func longPressReceived(sender: UILongPressGestureRecognizer) {
        gestureOutputTextLabel.text = "Long Press received";
    }
    func pinchReceived(sender: UIPinchGestureRecognizer) {
        gestureOutputTextLabel.text = "Pinch received with scale \(sender.scale)";
        
//        sender.scale = 1.0
    }


}

