//
//  CKInfiniteScroll.swift
//  abesCalculator
//
//  Created by Cerebro on 09/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class CKInfiniteScroll: UIView {
    
    var dataSource: CKInfiniteScrollDataSource
    var delegate:CKInfiniteScrollDelegate
    
    init() {
        
    }
    
    

}


protocol CKInfiniteScrollDataSource {
    func getResultsForPage(page:Int) -> [String]
}

protocol CKInfiniteScrollDelegate {
    
    func rowTapped(row:Int)
    
    optional func rowSwipedToRight(row:Int)
}
